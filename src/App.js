import logo from './logo.svg';
import '../src/weather.css'; 
import './App.css';
import React, { useEffect, useState ,Component} from "react";
import Cookies from 'universal-cookie';
import ReactDOM from "react-dom";
var api_url = "https://api.openweathermap.org/data/2.5/weather?q=";
var api_url2 = "&units=metric&appid=e857492510d175feff667d7dc5fcccfd";

class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      city: this.props.value
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.removeCard = this.removeCard.bind(this);

  }

  componentDidMount() {
  }

  fetchData = () => {
    var city = this.state.city +"";
    var page = api_url+city+api_url2;
    fetch(page)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result
          });
          var cod = this.state.items.cod
          if (cod == 200) {
            document.getElementById("lbl_val_inputCity").innerHTML = '';
            console.log(this.state.items)
            var item = this.state.items;
            const WCard = document.getElementById('galleryForm');
            var r = this.newWeatherCard(item);
            WCard && ReactDOM.render(
                    ReactDOM.createPortal(
                      r, 
                      WCard),
                    document.createElement('div')
                  );
            // ReactDOM.render( r, document.getElementById('galleryForm'));

          }else{
            console.log(city);
            if (city == "undefined" || city == "") {
              document.getElementById("lbl_val_inputCity").innerHTML = 'Cannot Be Empty.';
              //alert(this.state.items.message)
            }else if(cod == 404){
              document.getElementById("lbl_val_inputCity").innerHTML = 'City Not Found.';
            }
          }
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
      var list = this.state.items;
  }

  handleChange(event) {
        this.setState({city: event.target.value});
    }

  handleSubmit(event) {
    event.preventDefault();
    this.handleChange(event);
    this.fetchData();
    const city = this.state.city;
        this.mainInput.value = "";

  }

    removeCard = (event) => {
    var eleId = document.getElementById(event.currentTarget.id);
      console.log(eleId)
    eleId.parentNode.removeChild(eleId);
  }


    newWeatherCard = (item) => {
      const cookies = new Cookies();
      var terminalOutput;
      var index = cookies.get('card_index');
      var wcid = "weatherCard_"+index;
      var weatherStat = item.weather[0].main;
      var name = item.name;
      var temp = item.main.temp;
      var humidity = item.main.humidity;

      if (index == null)
        index = 0;
      cookies.set('card_index', +index + 1, { path: '/' });

      if (weatherStat.includes("Clear") ){
       return ( 
        <>
        <div id={wcid} class='col-4 mx-auto  mt-2 weatherCard' ><div class='div_remove'><button type="submit" onClick={(e) => this.removeCard(e)} id={wcid} class='btn_remove'>X</button></div><div class='col-10 mx-auto'><div id='lbl_cityDesc' class='lbl_cityDesc col-4 mt-2'><div class='row'>{name}</div><div class='row'> {weatherStat} </div><div class='row'>  {temp}°C</div><div class='row'>HUM {humidity}</div></div><div class='hot'><span class='sun'></span><span class='sunx'></span></div></div></div>
        </>
        );
      }else if (weatherStat.includes("Snow") )
       return ( 
        <><div id={wcid} class='col-4 mx-auto  mt-2 weatherCard' ><div class='div_remove'><button type="submit" onClick={(e) => this.removeCard(e)} id={wcid} class='btn_remove'>X</button></div><div class='col-10 mx-auto'><div id='lbl_cityDesc' class='lbl_cityDesc col-4 mt-2'><div class='row'>{name}</div><div class='row'>  {weatherStat}</div><div class='row'>  {temp}°C</div><div class='row'>HUM {humidity}</div></div><div class='stormy'><ul><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div></div></div></>);
      else if (weatherStat.includes("Rain") )
         return ( 
        <><div id={wcid} class='col-4 mx-auto  mt-2 weatherCard' ><div class='div_remove'><button type="submit" onClick={(e) => this.removeCard(e)} id={wcid} class='btn_remove'>X</button></div><div class='col-10 mx-auto'><div id='lbl_cityDesc' class='lbl_cityDesc col-4 mt-2'><div class='row'>{name}</div><div class='row'>  {weatherStat}</div><div class='row'>  {temp}°C</div><div class='row'>HUM {humidity}</div></div><div class='breezy'><ul><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul><span class='cloudr'></span></div></div></div></>);
      else //if (index == 5)
         return ( 
        <><div id={wcid} class='col-4 mx-auto  mt-2 weatherCard'><div class='div_remove'><button type="submit" onClick={(e) => this.removeCard(e)}  id={wcid} class='btn_remove'>X</button></div><div class='col-10 mx-auto'><div id='lbl_cityDesc' class='lbl_cityDesc col-4 mt-2'><div class='row'>{name}</div><div class='row'> {weatherStat} </div><div class='row'>  {temp}°C</div><div class='row'>HUM {humidity}</div></div><div class='cloudy div_weather'><span class='cloud'></span><span class='cloudx'></span></div></div></div></>
        );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else {
      // var list = {items}.items;
      // console.log(list)
      return (
        <>
        <div class="row col-12 f-navbar">
          <div class=" col-3 "></div>
            <div class=" col-6 mt-2">
            <form>
              <div class="col-12 ">
              <label id="lbl_val_inputCity" class="lbl_val_inputCity"></label>
              <input ref={(ref) => this.mainInput= ref} id="inputCity" type="text" value={this.state.value} onChange={this.handleChange} class="inputTx_card"  placeholder="Enter City..." required/></div>
              <div class="col-12 mt-1 mb-1" style={{textAlign: 'center'}}>
              <button onClick={this.handleSubmit} class="btn_addCard">
               Insert New
              </button></div></form>
            </div>
          <div class=" col-3"></div> 
        </div>
        
        </>
      );
    }
  }
}


const rootElement = document.getElementById("api");

export default MyComponent;
