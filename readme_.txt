Demo Online site : https://adzhen.github.io/weather_demo/

Instruction of WeatherDemo

1.install npm >= 5.6
2.install nodejs >= 10.16
3.Open CMD and redirect on weatherDemo project folder
 command: cmd :\ cd {project-path}
4.Run npm script 
 command: cmd : npm start
5.Now the site was Hosting on Localhost

weather_demo Features
1. Key in City Name 
2. Submit to Create a City Weather Details
3. Removeable from created City list

